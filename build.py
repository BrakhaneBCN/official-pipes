import glob
import json

import cerberus
import docker
import requests
import yaml
import re
import sys

import multiprocessing
from joblib import Parallel, delayed


def docker_image(field, value, error):
    try:
        client = docker.from_env()
        image = client.images.pull(value)
        if int(image.attrs['Size']) > 1073741824:
            error(field, "Docker images larger than 1GB not supported")
    except docker.errors.ImageNotFound:
        error(field, "Docker image not found")


REMOTE_METADATA = cerberus.Validator({
    'name': {'type': 'string', 'required': True},
    'description': {'type': 'string', 'required': True},
    'image': {'type': 'string', 'required': True, 'validator': docker_image},
    'repository': {'type': 'string', 'required': True},
    'icon': {'type': 'string', 'required': True},
    'maintainer': {'type': 'string', 'required': True},
    'tags': {'type': 'list', 'required': True}
})

LOCAL_METADATA = cerberus.Validator({
    'name': {'type': 'string', 'required': True},
    'description': {'type': 'string', 'required': True},
    'logo': {'type': 'string', 'required': False},
    'repositoryPath': {'type': 'string', 'required': True},
    'version': {'type': 'string', 'required': True},
    'vendor': {
        'type': 'dict',
        'schema': {
            'name': {'type': 'string', 'required': True},
            'website': {'type': 'string', 'required': True}
        },
        'required': False
    },
    'maintainer': {
        'type': 'dict',
        'schema': {
            'name': {'type': 'string', 'required': True},
            'website': {'type': 'string', 'required': True},
            'email': {'type': 'string', 'required': False}
        },
        'required': True
    },
    'yml': {'type': 'string', 'required': False}
})

FINAL_METADATA = cerberus.Validator({
    'name': {'type': 'string', 'required': True},
    'description': {'type': 'string', 'required': True},
    'logo': {'type': 'string', 'required': True},
    'repositoryPath': {'type': 'string', 'required': True},
    'version': {'type': 'string', 'required': True},
    'vendor': {
        'type': 'dict',
        'schema': {
            'name': {'type': 'string', 'required': True},
            'website': {'type': 'string', 'required': True}
        },
        'required': False
    },
    'maintainer': {
        'type': 'dict',
        'schema': {
            'name': {'type': 'string', 'required': True},
            'website': {'type': 'string', 'required': True},
            'email': {'type': 'string', 'required': False}
        },
        'required': True
    },
    'yml': {'type': 'string', 'required': True}
})


def get_latest_tag(repository_path):
    """
    Gets the latest tag of a repository (sort alphabetically)
     TODO: Improve this to handle proper sorting between semver
    :param repository_path: Repository path (ex: account/repo)
    :return: Latest tag
    """
    request_tags = requests.get(f"https://api.bitbucket.org/2.0/repositories/{repository_path}/refs/tags?sort=-name&pagelen=100")
    tag = (request_tags.json())['values'][0]['name']
    return tag


def get_repository_info(repository_path):
    """
    Retrieves repository information
    :param repository_path: Repository path (ex: account/repo)
    :return: JSON response of repository information
    """
    request_repo_info = requests.get(f"https://api.bitbucket.org/2.0/repositories/{repository_path}/")
    request_repo_info.raise_for_status()
    return request_repo_info.json()


def readme_exists(repository_path, version):
    """
    Determine if README.md file exists
    :param repository_path: Repository path (ex: account/repo)
    :param version: Reference (i.e. tag or commit)
    :return: True if exists, False otherwise
    """
    request = requests.head(f"https://bitbucket.org/{repository_path}/raw/{version}/README.md")
    return request.status_code == requests.codes.ok


def _extract_yml_definition(data):
    """
    Extracts the YAML definition
    :param data: Readme.md contents
    :return: code snippet (see tests for examples)
    """
    search_group = re.search(r"## YAML Definition(?:(?:.*?\n)*?)```yaml((?:.*?\n)*?)```$", data, re.MULTILINE)
    if search_group is None:
        return None
    code_snippet = (search_group.group(1)).lstrip()
    return code_snippet


def get_yml_definition(repository_path, version):
    """
    Determine if README.md file exists
    :param repository_path: Repository path (ex: account/repo)
    :param version: Reference (i.e. tag or commit)
    :return: True if exists, False otherwise
    """
    test = requests.get(f"https://bitbucket.org/{repository_path}/raw/{version}/README.md")
    test.encoding = "utf-8"
    yml_definition = _extract_yml_definition(test.text)
    return yml_definition


def get_pipe_metadata(repository_path, version):
    """
    Retrieves pipe metadata
    :param repository_path: Repository path (ex: account/repo)
    :param version: Reference (i.e. tag or commit)
    :return: YAML object
    """
    request = requests.get(f"https://bitbucket.org/{repository_path}/raw/{version}/pipe.yml")
    request.raise_for_status()
    return yaml.safe_load(request.text)


def validate(pipe_yml, infile):
    errors = []

    if not LOCAL_METADATA.validate(pipe_yml):
        errors.append(f"{infile} not valid: {str(LOCAL_METADATA.errors)}")

    repository_path = pipe_yml['repositoryPath']
    version = pipe_yml['version']

    # Fetch Repository information
    repo_info = get_repository_info(repository_path)

    if 'logo' not in pipe_yml:
        pipe_yml['logo'] = repo_info['links']['avatar']['href']

    # Get latest tag
    latest_tag = get_latest_tag(repository_path)
    if latest_tag != version:
        errors.append(f"{latest_tag} version available. Current version is {version}")

    # Fetch the README.md
    if not readme_exists(repository_path, version):
        errors.append(f"{infile} repository not valid: README.md not found")

    # TODO: check this until we make sure all code snippets are similar
    yml_definition = get_yml_definition(repository_path, version)
    if 'yml' in pipe_yml and yml_definition != pipe_yml['yml']:
        errors.append(f"{infile} yml field not equal")
    if 'yml' not in pipe_yml:
        pipe_yml['yml'] = yml_definition

    if yml_definition is None:
        errors.append(f"{infile} yml definition in README.md does not exist")

    # validate YAML file
    try:
        yaml.load(pipe_yml['yml'])
    except yaml.YAMLError as exc:
        errors.append(f"{infile} yml definition in README.md is not valid")

    # Fetch the pipe.yml (metadata)
    try:
        metadata_yml = get_pipe_metadata(repository_path, version)
        if not REMOTE_METADATA.validate(metadata_yml):
            errors.append(f"{infile} repository not valid: pipe.yml not valid: {str(REMOTE_METADATA.errors)}")
    except:
        errors.append(f"{infile} repository not valid: pipe.yml not found")

    if not FINAL_METADATA.validate(pipe_yml):
        errors.append(f"{infile} final metadata not valid: {str(FINAL_METADATA.errors)}")

    return errors


def process(infile):
    print(f"Start: {infile}")
    errors = []
    with open(infile, 'r') as stream:
        try:
            yaml_file = yaml.safe_load(stream)
            errors = validate(yaml_file, infile)
            return {
                'metadata': {
                    'file': infile,
                    'valid': len(errors) == 0,
                    'errors': errors
                },
                'data': yaml_file
            }
        except yaml.YAMLError as exc:
            print(exc)
        finally:
            print(f"Finish: {infile}. Success: " + str(len(errors) == 0))


def main():
    path = 'pipes/*.yml'
    num_cores = min(multiprocessing.cpu_count(), 8)

    if len(sys.argv) > 1:
        path = sys.argv[1]

    list_files = sorted(glob.glob(path))

    results = Parallel(n_jobs=num_cores)(delayed(process)(i) for i in list_files)

    data = list(map(lambda x: x['data'], results))
    metadata = list(map(lambda x: x['metadata'], results))

    # TODO: do not generate json files who contains errors
    print("\nSummary:")
    print(*metadata, sep='\n')
    with open('pipes.json', 'w', encoding='utf8') as outfile:
        json.dump(data, outfile, ensure_ascii=False)


if __name__ == '__main__':
    main()
